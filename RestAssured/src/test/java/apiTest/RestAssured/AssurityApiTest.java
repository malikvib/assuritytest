package apiTest.RestAssured;

import static org.junit.Assert.*;

import org.junit.Test;

public class AssurityApiTest 
{
	
    AssurityApiMain apiMain = new AssurityApiMain(); //Calling constructor
  
 //  Test to verify the Name is Carbon credits  
	@Test
	public void verifyNameIsCarbonCreditsTest() {     

		 String name = apiMain.getResponseBody().get("Name");
		 assertEquals(name, "Carbon credits"); // Assertion for name equal to Carbon credits
	}

 // Test to verify CanReList is set to True
	@Test
	public void verifyCanRelistIsSetToTrueTest() {
			 
		 boolean canRelist = apiMain.getResponseBody().get("CanRelist");
		 assertTrue(canRelist);  // Assertion to check is canReList is set to true
	}

 // Test to verify Promotion with name Gallery contains "2x larger image" text"	
	@Test
	public void VerifyPromotionNameGalleryContainsText2xLargerImageTest() {     
		
		 String promotionName = apiMain.getResponseBody().get("Promotions[1].Name");
		 String description = apiMain.getResponseBody().get("Promotions[1].Description");
		 assertEquals(promotionName,"Gallery"); // Assertion for Promotionname equal to Gallery
		 assertEquals(description.contains("2x larger image"),true); // Assertion for Gallery description contains text 2x larger image
		
	}


}
