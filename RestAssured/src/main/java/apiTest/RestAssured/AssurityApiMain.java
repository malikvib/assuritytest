package apiTest.RestAssured;

import static io.restassured.RestAssured.given;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AssurityApiMain 
{
	
	// Common method to return the Response in form of JsonPath
	public JsonPath getResponseBody() {	        
	
	 Response response = given()
		        .given()
		        .contentType("application/json")
		        .when()
		        .get("https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false");
	 
	 JsonPath jsonPathEvaluator = response.jsonPath();
	 return jsonPathEvaluator;
		
	}
}
