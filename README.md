## Project usage instructions using REST Assured

## Pre-Requisites

1. Use JDK V1.8 and set JAVA_HOME as system variable under environment variables.
2. Use Maven version 3.6.3. Set M2_HOME and MAVEN_HOME as system variable under environment variables and point the value of these variables
   as path to Mavan installation directory.
3. Add %MAVEN_HOME%\bin to Path variable.
4. Add %JAVA_HOME%\bin to Path variable.
5. Use JBoss developer studio as developer tool(Preferably).
6. GIT must be installed.

## Executing the tests

1. Create a folder in your machine at any location with a name say "AssurityTest"
2. Now go the folder location (Use GIT Bash shell) created in step 1 through GIT Bash terminal and run the following command
   git clone https://bitbucket.org/malikvib/assuritytest.git
   Note:- You can use any GIT GUI client as well for cloning the project above. 
3. Import the project folder that you checked out (cloned) in the above step in IDE tool(preferably JBoss Developer Studio).
4. Right click on "RestAssured" project after importing in IDE and select Run as -- > "Maven install" and then "Maven test" OR
5. In order to run the tests from command terminal perform the below steps.
6. Right click on "RestAssured" project in IDE and select "Show in Local Terminal" --> Terminal.
7. In the terminal window that comes after previous step type the command "mvn install test" and press Enter.
8. You should be able to see the test results in output. OR 
9. Right click on "RestAssured" project in IDE and select "Run as Junit Test"





 

